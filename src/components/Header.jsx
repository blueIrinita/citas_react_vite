function Header(){

    return(
        <h1 className="text-5xl mx-auto text-center md:w-1/2">
            Seguimiento pacientes 
            <span className="text-pink-300" > Veterinaria</span>
        </h1>
    )
}

export default Header;