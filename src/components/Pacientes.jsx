import React from 'react'

const Pacientes = ({mascota, setPaciente, deletePaciente}) => {
    const handlerDelete = () => {
        const response = confirm('Deseas eliminar este paciente?')
        if(response){
            deletePaciente(mascota.id)
        }
    }
  return (
    <div className='m-3 bg-white shadow-md px-5 py-10 rounded-xl'>
        <p className='font-bold  mb-3 text-gray-700 uppercase'>
            Nombre: {''}
            <span className='font-normal normal-case'>{mascota.nombre}</span>
        </p>
        <p className='font-bold  mb-3 text-gray-700 uppercase'>
            Propietario: {''}
            <span className='font-normal normal-case'>{mascota.propietario}</span>
        </p>
        <p className='font-bold  mb-3 text-gray-700 uppercase'>
            Email: {''}
            <span className='font-normal normal-case'>{mascota.email}</span>
        </p>
        <p className='font-bold  mb-3 text-gray-700 uppercase'>
            fecha alta: {''}
            <span className='font-normal normal-case'>{mascota.fecha}</span>
        </p>
        <p className='font-bold  mb-3 text-gray-700 uppercase'>
            Sintomas: {''}
            <span className='font-normal normal-case'>{mascota.sintomas}</span>
        </p>
        <div className='flex'>
            <button
                className='bg-pink-300 hover:bg-pink-500 cursor-pointer transition-shadow w-1/2 rounded-lg mt-3 mx-3 uppercase'
                type='button'
                onClick={() => setPaciente(mascota)}
            >Editar</button>
            <button
                className='bg-red-300 hover:bg-red-500 cursor-pointer transition-shadow w-1/2 rounded-lg mt-3 mx-3 uppercase'
                type='button'
                onClick={handlerDelete}
            >Eliminar</button>
        </div>
      </div>
  )
}

export default Pacientes
