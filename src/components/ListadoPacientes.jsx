import {useEffect} from 'react'
import Pacientes from './Pacientes'


const ListadoPacientes = ({pacientes, setPaciente, deletePaciente}) => {

    useEffect( ()  => {
        if(pacientes.length > 0){
            console.log('NUevo paciente')
        }
        
    }, [pacientes])


  return (
    <div className='md:w-1/2 lg:w-3/5 md:h-screen overflow-y-scroll'>
        {
            pacientes && pacientes.length!==0 
            ? (
                <>
                    <h2 className='text-3xl text-center font-black'>Listado pacientes</h2>
                    <p className='text-xl mt-5 mb-10 text-center'>
                        Administra tus {''}
                        <span className='text-pink-300'>Pacientes y Citas</span>
                    </p>
                    { pacientes.map( mascota => <Pacientes mascota={mascota} key={mascota.id} setPaciente={setPaciente} deletePaciente={deletePaciente} /> )}
                </>
            )
            :(
                <>
                    <h2 className='text-3xl text-center font-black'>No hay pacientes</h2>
                    <p className='text-xl mt-5 mb-10 text-center'>
                        Comienza agregandolos {''}
                        <span className='text-pink-300'>y apareceran en este lugar</span>
                    </p>
                </>
            )
        }
      
    </div>
  )
}

export default ListadoPacientes
