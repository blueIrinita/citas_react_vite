import {useState, useEffect} from 'react'
import Error from './Error';

const Formulario = ({setPacientes, pacientes, paciente, setPaciente}) => {

    const generarId = () => {
        const fecha = Date.now().toString(36)
        const random = Math.random().toString(36).substr(2)
        return random + fecha
    }

    const [cliente, setCliente] = useState({
        nombre: '',
        propietario: '',
        email: '',
        fecha: '',
        sintomas: '', 
    });

    const [error, setError] = useState(false)

    useEffect( ()=>{
        if(Object.keys(paciente).length > 0){
            setCliente(paciente)
        }
    },[paciente])

    const handleSubmit = (e) => {
        e.preventDefault();
        if( Object.values(cliente).some(x => x === '')){
            console.log('al menos 1 campo esta vacio');
            setError(true)
        }else{
            setError(false)
            
            if(paciente.id){
                const newList = pacientes.map((mascota) => {
                    if(mascota.id === paciente.id){
                        const actualizado = {
                            id: mascota.id, 
                            nombre: cliente.nombre,
                            propietario: cliente.propietario,
                            email: cliente.email,
                            fecha: cliente.fecha,
                            sintomas: cliente.sintomas,
                        }
                        return actualizado
                    }else{
                        return mascota
                    }
                })
                console.log(newList);
                setPacientes(newList)
                setPaciente({})
            }else{
                //creando nuevo registro
                const objetoCliente = cliente
                objetoCliente.id = generarId()
                setPacientes([...pacientes, objetoCliente])
            }
            setCliente({
                nombre: '',
                propietario: '',
                email: '',
                fecha: '',
                sintomas: '',
            })
        }
        
        
    }

  return (
    <div className='md:w-1/2 lg:w-2/5 mx-5'>
      <h1 className='font-black text-3xl text-center'>Seguimiento pacientes</h1>
      <p className='text-lg mt-5 text-center mb-10'>
        Añade pacientes y {''}
        <span  className='text-pink-300 font-bold'>Administralos</span>
      </p>
      <form 
        onSubmit={handleSubmit}
        className='bg-white shadow-md rounded-lg py-10 px-5 mb-7'
        >
            {error && (
                <Error  mensaje={'Todos los campos son obligatorios'} />
            )}
        <div className='mb-5'>
            <label htmlFor='mascota'  className='block text-gray-700 uppercase font-bold'>
                Nombre mascota
            </label>
            <input 
                id='mascota'
                type="text"
                placeholder='nombre mascota'
                className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                value={cliente.nombre}
                onChange={(value) => setCliente({...cliente, nombre: value.target.value})}
            />
        </div>
        <div className='mb-5'>
            <label htmlFor='owner'  className='block text-gray-700 uppercase font-bold'>
                Nombre propietario
            </label>
            <input 
                id='owner'
                type="text"
                placeholder='nombre dueño'
                className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                value={cliente.propietario}
                onChange={(value) => setCliente({...cliente, propietario: value.target.value})}
            />
        </div>
        <div className='mb-5'>
            <label htmlFor='email'  className='block text-gray-700 uppercase font-bold'>
                email
            </label>
            <input 
                id='email'
                type="email"
                placeholder='Email contacto'
                className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                value={cliente.email}
                onChange={(value) => setCliente({...cliente, email: value.target.value})}
            />
        </div>
        <div className='mb-5'>
            <label htmlFor='data'  className='block text-gray-700 uppercase font-bold'>
                Alta
            </label>
            <input 
                id='data'
                type="date"
                className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                value={cliente.fecha}
                onChange={(value) => setCliente({...cliente, fecha: value.target.value})}
            />
        </div>
        <div className='mb-5'>
            <label htmlFor='sintomas'  className='block text-gray-700 uppercase font-bold'>
                Sintomas
            </label>
            <textarea
                id='sintomas'
                placeholder='Describe los sintomas'
                className='border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md'
                value={cliente.sintomas}
                onChange={(value) => setCliente({...cliente, sintomas: value.target.value})}
            />
        </div>
        <input 
            type="submit"
            value={paciente.id ? "Editar paciente" : "Agregar paciente"}
            className='bg-pink-300 p-3 text-white uppercase font-bold w-full hover:bg-pink-500 cursor-pointer transition-colors'
        />
      </form>
    </div>
  )
}

export default Formulario
