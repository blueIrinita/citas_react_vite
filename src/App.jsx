import { useState, useEffect } from "react"
import Header from "./components/Header"
import Formulario from "./components/Formulario"
import ListadoPacientes from "./components/ListadoPacientes"


function App() {

  const [pacientes, setPacientes] = useState(JSON.parse(localStorage.getItem('pacientes')) ?? [])
  const [paciente, setPaciente] = useState({})

  const deletePaciente = id => {
    const newList = pacientes.filter( pet => pet.id !== id )
    setPacientes(newList)
  }

  useEffect(()=> {
    localStorage.setItem('pacientes', JSON.stringify(pacientes))
  },[pacientes])

  

  return (
    <div className="container mx-auto mt-20">
      <Header/>
      <div className="md:flex mt-12">
        <Formulario 
          setPacientes={value => setPacientes(value)} 
          paciente={paciente}
          pacientes={pacientes}
          setPaciente={setPaciente}
          />
        <ListadoPacientes 
          pacientes={pacientes} 
          setPaciente={setPaciente} 
          deletePaciente={deletePaciente}
        />
      </div>
      
    </div>
  )
}

export default App
